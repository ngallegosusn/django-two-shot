# Generated by Django 4.2.1 on 2023-06-01 18:41

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0004_receipt_category_alter_receipt_account"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(),
        ),
    ]
